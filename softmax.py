#################################################################################
# Trabalho 2 - 24/09/2018
#
# Aluno: Felipe S. P. Carvalho - RA: 146040
# Aluno: Yuri Catarino Delgado - RA: 158526
#
#################################################################################

"""
python3 softmax.py files/fashion-mnist_train.csv 1000 .000001
"""

from main import get_datasets, import_data, hist, line, _format
import scipy.sparse
import pandas as pd
import numpy as np
import sys

def softmax(z):
    return (np.exp(z).T / np.sum(np.exp(z),axis=1)).T

def oneHot(data):
    rows = data.shape[0]
    one_hot = scipy.sparse.csr_matrix((np.ones(rows), (data, np.array(range(rows)))))
    return np.array(one_hot.todense()).T

def gradient(x, y, theta, hist_cost):
    m = x.shape[0]
    z = x.dot(theta)
    h = softmax(z)
    oh_y = oneHot(y)
    cost = (-1 / m) * np.sum(oh_y * np.log(h) + (1 - oh_y) * np.log(1 - h)) # Slide
    # cost = (-1 / m) * np.sum(oh_y * np.log(h)) + (1 / 2) * np.sum(theta * theta) # Interweb
    grad = (-1 / m) * x.T.dot(oh_y - h) + theta
    hist_cost.append(cost)
    return grad

def apply(x, y, theta):
    # Prob for each class
    z = softmax(x.dot(theta))
    # Most prob class
    max = np.argmax(z, axis=1)
    comp = max == y
    comp = np.array(comp, dtype=int)
    return sum(comp)/y.shape[0]


# Call methods
if __name__ == "__main__":

    if len(sys.argv) < 2:
        print("First argument is missing: you must insert the training file path")
        sys.exit()

    training_file = sys.argv[1]
    iterations = int(sys.argv[2])
    alpha = float(sys.argv[3])

    # Training set
    training_set = import_data(training_file)
    x_training, y_training, x_validation, y_validation = get_datasets(training_set)
    theta = np.zeros([x_training.shape[1],len(np.unique(y_training))])

    hist_cost = []
    for i in range(0, iterations):
        print("Iteration: ", i+1, " of ", iterations)
        grad = gradient(x_training, y_training, theta, hist_cost)
        theta -= alpha * grad

    ac_training = apply(x_training, y_training, theta)
    ac_validation = apply(x_validation, y_validation, theta)
    print("Training: ", _format(ac_training))
    print("Validation: ", _format(ac_validation))

    # Data visualization
    hist(y_training, x_label='Classes', y_label='Quantity', title='Class distribution')
    line(hist_cost, x_label='Iterations', y_label='Cost', title='Cost')
