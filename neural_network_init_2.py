import numpy as np
import sys
from main import get_datasets, import_data, hist, line, _format


def sigmoid(x):
    return 1.0/(1+ np.exp(-x))

def sigmoid_derivative(x):
    return x * (1.0 - x)

class NeuralNetwork:
    def __init__(self, x, y):
        self.input      = x
        self.weights1   = np.random.rand(self.input.shape[1],4) 
        self.weights2   = np.random.rand(4,1)                 
        self.y          = y
        self.output     = np.zeros(self.y.shape)

    def feedforward(self):
        self.layer1 = sigmoid(np.dot(self.input, self.weights1))
        self.output = sigmoid(np.dot(self.layer1, self.weights2))

    def backprop(self):
        # application of the chain rule to find derivative of the loss function with respect to weights2 and weights1
        d_weights2 = np.dot(self.layer1.T, (2*(self.y - self.output) * sigmoid_derivative(self.output)))
        d_weights1 = np.dot(self.input.T,  (np.dot(2*(self.y - self.output) * sigmoid_derivative(self.output), self.weights2.T) * sigmoid_derivative(self.layer1)))

        # update the weights with the derivative (slope) of the loss function
        self.weights1 += d_weights1
        self.weights2 += d_weights2


if __name__ == "__main__":

    # X = np.array([[0,0,1],
    #               [0,1,1],
    #               [1,0,1],
    #               [1,1,1]])
    # y = np.array([[0],[1],[1],[0]])

    if len(sys.argv) < 2:
        print("First argument is missing: you must insert the training file path")
        sys.exit()

    training_file = sys.argv[1]

    training_set = import_data(training_file)
    x_training, y_training, x_validation, y_validation = get_datasets(training_set, add_one=False)
    y_training = np.transpose([y_training]) # Column matrix format

    nn = NeuralNetwork(x_training, y_training)

    for i in range(100):
        print("# " + str(i) + "\n")
        print("Input: \n" + str(x_training))
        print("Actual Output: \n" + str(y_training))
        print("Predicted Output: \n" + str(nn.output))
        nn.feedforward()
        nn.backprop()
