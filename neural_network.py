#################################################################################
# Trabalho 2 - 24/09/2018
#
# Aluno: Felipe S. P. Carvalho - RA: 146040
# Aluno: Yuri Catarino Delgado - RA: 158526
#
#################################################################################

import numpy as np

class Neural_Network(object):
    def __init__(self, x):
        # weights
        eps = 10
        self.w1 = np.random.randn(x.shape[1],32)
        self.w2 = np.random.randn(32,10)

    def forward(self, x):
        # forward propagation through our network
        self.l1 = x
        self.l2 = self._sigmoid(self.l1.dot(self.w1))
        self.l3 = self._sigmoid(self.l2.dot(self.w2))
        return self.l3

    def _sigmoid(self, s):
        return .5 * (1 + np.tanh(.5 * s)) # https://stackoverflow.com/questions/26218617/runtimewarning-overflow-encountered-in-exp-in-computing-the-logistic-function

    # derivative of sigmoid
    def _sigmoid_prime(self, s):
        return s * (1 - s)

    def backward(self, x, y, output):
        # backward propagate through the network
        l3_error = output - y
        l3_delta = l3_error * self._sigmoid_prime(output)

        # Hidden layer error contribution
        l2_error = l3_delta.dot(self.w2.T)
        l2_delta = l2_error * self._sigmoid_prime(self.l2)

        return self.l1.T.dot(l2_delta), self.l2.T.dot(l3_delta)

    def train(self, x, y):
        output = self.forward(x)
        return self.backward(x, y, output)
