#################################################################################
# Trabalho 2 - 24/09/2018
#
# Aluno: Felipe S. P. Carvalho - RA: 146040
# Aluno: Yuri Catarino Delgado - RA: 158526
#
#################################################################################

import matplotlib.pyplot as plt
import scipy.sparse
import pandas as pd
import numpy as np
import math
import time
import sys
from copy import deepcopy


def hist(data, bins=40, x_label='', y_label='', title=''):
    plt.hist(data, bins)
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.title(title)
    plt.show()

def line(x_data, y_data='', x_label='', y_label='', title=''):
    plt.plot(x_data, y_data)
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.title(title)
    plt.show()

def _format(val):
    return "{:.2%}".format(val)

def oneHot(data):
    rows = data.shape[0]
    one_hot = scipy.sparse.csr_matrix((np.ones(rows), (data, np.array(range(rows)))))
    return np.array(one_hot.todense()).T

# Subroutine that imports data
def import_data(filename):
    data = pd.read_csv(filename)
    return data

def get_datasets(data, add_one=True):
    rows, cols = data.shape
    data_training = data.loc[:(0.8 * rows), :]
    data_validation = data.loc[(0.8 * rows):, :]

    x_training = data_training.loc[:, data.columns != 'label']
    if add_one: x_training.insert(0, 'x0', 1)
    y_training = data_training.loc[:, 'label']

    x_validation = data_validation.loc[:, data.columns != 'label']
    if add_one: x_validation.insert(0, 'x0', 1)
    y_validation = data_validation.loc[:, 'label']

    return x_training.values, y_training.values, x_validation.values, y_validation.values


def fit(x, y, theta, n_iter, alpha):
    m = x.shape[0]
    for _ in range(n_iter):
        z = x.dot(theta)
        erros = y - _sigmoid(z)
        theta += alpha / m * erros.dot(x) # implicit gradient
    return theta


def fit_ova(x, y, theta, n_iter, alpha):
    x_rows, x_cols = x.shape
    num_classes = np.max(y) + 1

    for i in np.unique(y):
        print('Logistic classifier training number %d/%dth...' % (i + 1, num_classes))
        y_copy = np.where(y == i, 1, 0)
        w = np.ones(x_cols)

        for j in range(n_iter):
            z = x.dot(w)
            errors = y_copy - _sigmoid(z)
            w += alpha / x_rows * errors.dot(x)
        theta.append((w, i))

    return theta


def predict(x, theta):
    z = x.dot(theta)
    return np.where(_sigmoid(z) >= .5, 1, 0)


def predict_ova(x, theta):
    return [predict_one_ova(i, theta) for i in x]


def predict_one_ova(x, theta):
    return max((x.dot(w), c) for w, c in theta)[1]


def score_ova(x, y, theta):
    return sum(predict_ova(x, theta) == y) / len(y)


def _sigmoid(z):
    return .5 * (1 + np.tanh(.5 * z)) # https://stackoverflow.com/questions/26218617/runtimewarning-overflow-encountered-in-exp-in-computing-the-logistic-function


def get_data_dimensions(data):
    rows, cols = data.shape
    return rows, cols


def get_datasets(data):
    x = data.loc[:, data.columns != 'label']
    y = data.loc[:, 'label']
    return x, y


# TODO
# Verificar se o resultado para matriz theta inicializada com 1 é uma matriz de 1.0 mesmo
def hyp(theta, x):
    return 1 / ( 1 + math.e ** (-(x.dot(theta))))


def logistic_regression(x, y):
    num_classes = np.max(y) + 1
    class_models = []
    hist_cost_models = []

    for i in range(num_classes):
        print('Logistic classifier training number %d/%dth:\n' % (i + 1, num_classes))
        y_logistic = deepcopy(y)
        id_i = (y_logistic == i)
        y_logistic[id_i] = 1
        y_logistic[-id_i] = 0
        theta, hist_cost = logistic_regression_train_alg(x, y_logistic)
        class_models.append(theta)
        hist_cost_models.append(hist_cost)


def logistic_regression_train_alg(x, y, num_iters=1000, alpha=0.001):
    rows, cols = x.shape

    # Initializing theta
    theta = np.ones(cols)[np.newaxis]
    hist_cost = []

    theta = theta.T
    for i in range(num_iters):
        loss = hyp(theta, x)[0] - y
        gradient = x.T.dot(loss) / rows
        gradient = gradient.values[np.newaxis] # Let gradient on the same dimension as theta
        gradient = gradient.T
        theta -= alpha * gradient
        cost = np.sum(loss ** 2)
        hist_cost.append(cost)
        # print('iteration %d/%d: loss %f' % (i, num_iters, loss))

    return theta, hist_cost


def logistic_regression_predict():
    return True


# Call methods
if __name__ == "__main__":

    if len(sys.argv) < 2:
        print("First argument is missing: you must insert the training file path")
        sys.exit()

    training_file = sys.argv[1]

    # Training set
    training_set = import_data(training_file)
    # x, y, theta = get_datasets(training_set)

    x = np.array([[-2, 2],[-3, 0],[2, -1],[1, -4]])
    x = np.insert(x, 0, 1, axis=1)
    y = np.array([1,1,0,0])
    x_rows, x_cols = x.shape
    theta = np.ones(x_cols)
    theta = fit(x, y, theta, 50, 0.1)
    print(predict(x, theta))

    x_training, y_training, x_validation, y_validation = get_datasets(training_set)

    thetaOvA = []
    print("Performing Logistic Regression...")

    start = time.time()
    thetaOvA = fit_ova(x_training, y_training, thetaOvA, 100, 0.1)
    end = time.time()

    print("Logistic Regression complete in %d seconds" % (end - start))

    print("Calculating score for validation data...")
    print("Score: " + "{:.2%}".format(score_ova(x_validation, y_validation, thetaOvA)))
