#################################################################################
# Trabalho 2 - 24/09/2018
#
# Aluno: Felipe S. P. Carvalho - RA: 146040
# Aluno: Yuri Catarino Delgado - RA: 158526
#
#################################################################################

import matplotlib.pyplot as plt
import scipy.sparse
import pandas as pd
import numpy as np
import time
import sys


def hist(data, bins=40, x_label='', y_label='', title=''):
    plt.hist(data, bins)
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.title(title)
    plt.show()


def line(x_data, y_data='', x_label='', y_label='', title=''):
    plt.plot(x_data, y_data)
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.title(title)
    plt.show()


def _format(val):
    return "{:.2%}".format(val)


def oneHot(data):
    rows = data.shape[0]
    one_hot = scipy.sparse.csr_matrix((np.ones(rows), (data, np.array(range(rows)))))
    return np.array(one_hot.todense()).T


# Subroutine that imports data
def import_data(filename):
    data = pd.read_csv(filename)
    return data


def get_datasets(data, add_one=True):
    rows, cols = data.shape
    data_training = data.loc[:(0.5 * rows), :]
    data_validation = data.loc[(0.5 * rows):, :]

    x_training = data_training.loc[:, data.columns != 'label']
    if add_one: x_training.insert(0, 'x0', 1)
    y_training = data_training.loc[:, 'label']

    x_validation = data_validation.loc[:, data.columns != 'label']
    if add_one: x_validation.insert(0, 'x0', 1)
    y_validation = data_validation.loc[:, 'label']

    return x_training.values, y_training.values, x_validation.values, y_validation.values


def fit(x, y, theta, n_iter, alpha):
    m = x.shape[0]
    for _ in range(n_iter):
        z = x.dot(theta)
        erros = y - _sigmoid(z)
        theta += alpha / m * erros.dot(x) # implicit gradient
    return theta


def fit_ova(x, y, theta, n_iter, alpha, hist_cost):
    x_rows, x_cols = x.shape
    num_classes = np.max(y) + 1

    for i in np.unique(y):
        print('Logistic classifier training number %d/%dth...' % (i + 1, num_classes))
        y_copy = np.where(y == i, 1, 0)
        w = np.ones(x_cols)

        for j in range(n_iter):
            z = x.dot(w)
            errors = y_copy - _sigmoid(z)
            w += alpha / x_rows * errors.dot(x)
        theta.append((w, i))

    return theta


def predict(x, theta):
    z = x.dot(theta)
    return np.where(_sigmoid(z) >= .5, 1, 0)


def predict_ova(x, theta):
    return [predict_one_ova(i, theta) for i in x]


def predict_one_ova(x, theta):
    return max((x.dot(w), c) for w, c in theta)[1]


def score_ova(x, y, theta):
    return sum(predict_ova(x, theta) == y) / len(y)


def _sigmoid(z):
    return .5 * (1 + np.tanh(.5 * z)) # https://stackoverflow.com/questions/26218617/runtimewarning-overflow-encountered-in-exp-in-computing-the-logistic-function


# Call methods
if __name__ == "__main__":

    if len(sys.argv) < 3:
        print("First argument is missing: you must insert the training file path")
        sys.exit()

    training_file = "files/fashion-mnist_train.csv"
    iterations = int(sys.argv[1])
    alpha = float(sys.argv[2])

    # Training set
    training_set = import_data(training_file)
    x_training, y_training, x_validation, y_validation = get_datasets(training_set)

    hist_cost = []
    thetaOvA = []
    print("Performing Logistic Regression...")

    start = time.time()
    thetaOvA = fit_ova(x_training, y_training, thetaOvA, iterations, alpha, hist_cost)
    end = time.time()

    print("Logistic Regression complete in %d seconds" % (end - start))

    print("Calculating score for validation data...")
    print("Score: " + "{:.2%}".format(score_ova(x_validation, y_validation, thetaOvA)))

    # line(hist_cost, x_label='Iterations', y_label='Cost', title='Cost')

