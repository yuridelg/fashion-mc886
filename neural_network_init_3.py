import numpy as np
import sys
from main import get_datasets, import_data, hist, line, _format
import matplotlib
import matplotlib.pyplot as plt
from sklearn.metrics import classification_report, confusion_matrix
from scipy.interpolate import interp1d


def sigmoid(z):
    # s = 1 / (1 + np.exp(-z))
    return .5 * (1 + np.tanh(.5 * z))  # https://stackoverflow.com/questions/26218617/runtimewarning-overflow-encountered-in-exp-in-computing-the-logistic-function


def compute_multiclass_loss(Y, Y_hat):
    L_sum = np.sum(np.multiply(Y, np.log(Y_hat)))
    m = Y.shape[1]
    L = -(1/m) * L_sum

    return L


if __name__ == "__main__":

    if len(sys.argv) < 2:
        print("First argument is missing: you must insert the training file path")
        sys.exit()

    training_file = sys.argv[1]

    training_set = import_data(training_file)
    x_training, y_training, x_validation, y_validation = get_datasets(training_set, add_one=False)

    x_training = x_training.T
    x_training = x_training / 255
    x_validation = x_validation.T
    x_validation = x_validation / 255

    num_of_classes = 10

    examples = y_training.shape[0]
    y_training = y_training.reshape(1, examples)
    y_new = np.eye(num_of_classes)[y_training.astype('int32')]
    y_new = y_new.T.reshape(num_of_classes, examples)
    y_training = y_new

    examples_validation = y_validation.shape[0]
    y_validation = y_validation.reshape(1, examples_validation)
    y_validation_new = np.eye(num_of_classes)[y_validation.astype('int32')]
    y_validation_new = y_validation_new.T.reshape(num_of_classes, examples_validation)
    y_validation = y_validation_new

    n_x = x_training.shape[0]
    n_h = 64
    learning_rate = 1

    W1 = np.random.randn(n_h, n_x)
    b1 = np.zeros((n_h, 1))
    W2 = np.random.randn(num_of_classes, n_h)
    b2 = np.zeros((num_of_classes, 1))

    X = x_training
    Y = y_training

    for i in range(1000):

        Z1 = np.matmul(W1, X) + b1
        A1 = sigmoid(Z1)
        Z2 = np.matmul(W2, A1) + b2
        A2 = np.exp(Z2) / np.sum(np.exp(Z2), axis=0)

        cost = compute_multiclass_loss(Y, A2)

        dZ2 = A2 - Y
        dW2 = (1. / n_x) * np.matmul(dZ2, A1.T)
        db2 = (1. / n_x) * np.sum(dZ2, axis=1, keepdims=True)

        dA1 = np.matmul(W2.T, dZ2)
        dZ1 = dA1 * sigmoid(Z1) * (1 - sigmoid(Z1))
        dW1 = (1. / n_x) * np.matmul(dZ1, X.T)
        db1 = (1. / n_x) * np.sum(dZ1, axis=1, keepdims=True)

        W2 = W2 - learning_rate * dW2
        b2 = b2 - learning_rate * db2
        W1 = W1 - learning_rate * dW1
        b1 = b1 - learning_rate * db1

        if i % 100 == 0:
            print("Epoch", i, "cost: ", cost)

    print("Final cost:", cost)

    Z1 = np.matmul(W1, x_validation) + b1
    A1 = sigmoid(Z1)
    Z2 = np.matmul(W2, A1) + b2
    A2 = sigmoid(Z2)

    predictions = (A2 > .5)[0, :]
    labels = (y_validation == 1)[0, :]

    print(confusion_matrix(predictions, labels))
    print(classification_report(predictions, labels))