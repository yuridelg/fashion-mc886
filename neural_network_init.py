#################################################################################
# Trabalho 2 - 24/09/2018
#
# Aluno: Felipe S. P. Carvalho - RA: 146040
# Aluno: Yuri Catarino Delgado - RA: 158526
#
#################################################################################

"""
python3 neural_network_init.py files/fashion-mnist_train.csv 100
"""

from main import get_datasets, import_data, hist, line, _format, oneHot
from neural_network import Neural_Network
from scipy.interpolate import interp1d
import numpy as np
import sys

# TODO
# Função de acurácia pra saber como está - acurácia para cada eṕoca
# Validar esta merda
# Tá meio lento, mas não tem muito o que fazer em estocástico, é isso mesmo. UMa solução é usar mini batches grandes

# Call methods
if __name__ == "__main__":

    if len(sys.argv) < 3:
        print("First argument is missing: you must insert the training file path")
        sys.exit()

    training_file = sys.argv[1]
    epoch = int(sys.argv[2])
    alpha = float(sys.argv[3])

    # Training set
    training_set = import_data(training_file)
    x_training, y_training, x_validation, y_validation = get_datasets(training_set, add_one=False)

    x_training = x_training / 255
    y_training = oneHot(y_training)

    NN = Neural_Network(x_training)

    print("Input: \n" + str(x_training))
    print("Actual Output: \n" + str(y_training))

    for i in range(epoch):
        l2_ac = l3_ac = 0
        print("# " + str(i) + "\n")
        for k in range(x_training.shape[0]):
            # print("Predicted Output: \n" + str(NN.forward(x_training[k:k+1])))
            ac2, ac3 = NN.train(x_training[k:k+1], y_training[k:k+1])
            l2_ac += ac2
            l3_ac += ac3

        print("Loss: \n" + str(np.mean(np.square(y_training[k:k+1] - NN.forward(x_training[k:k+1]))))) # mean sum squared loss
        print("Predicted Output: \n" + str(NN.forward(x_training[k:k+1])))

        # Update weights
        NN.w1 -= alpha * l2_ac / x_training.shape[0]   # input -> hidden
        NN.w2 -= alpha * l3_ac / x_training.shape[0]   # hidden -> output
